# calc.rb

# output method
# decorates output messages

def output(str)
  puts "=> #{str} <="
end

# Regex Input Validation
# Stops Strings from Adding to 0

def input_validation?(input)
	 !/^\d+$/.match(input).nil?
end

# Input Request
# Requests integers and validates entry

def get_int(input)
	output "Input an integer"
	input = gets.chomp
	if input_validation?(input)
		input = input.to_i
	else
		output "It needs to be an integer, try again!"
		get_int(input)
	end
end

# Get first integer
int1 = get_int(int1)

# Get second integer
int2 = get_int(int2)

# Select an operation
output "Choose an operation: \n1.) Addition \n2.) Subtraction \n3.) Multiplication \n4.) Division \n5.) Modulus"
operation = gets.chomp

# If block to make calculation and display error/result.

if operation == "1"
	puts "The result is #{int1.to_i + int2.to_i}"
elsif operation == "2"
	puts "The result is #{int1.to_i - int2.to_i}"
elsif operation == "3"
	puts "The result is #{int1.to_i * int2.to_i}"
elsif operation == "4"
	if int2 == 0
		puts "Dividing by 0 blows up the universe, you've killed all of your friends and the rest of humanity.  Good job."
		exit
	else
		puts "The result is #{int1.to_f / int2.to_f}"
	end
elsif operation == "5"
	if int2 == "0"
		puts "Dividing by 0 blows up the universe, you've killed all of your friends and the rest of humanity.  Good job."
	else
			puts "The result is #{int1.to_i % int2.to_i}"
	end
else
	output "Can't you follow instructions?? You only had 5 options, how can you screw that up?!"
end 